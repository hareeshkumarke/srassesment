## Deployment of sample App with Nginx

- Create infra with Terraform

  

  ```
  cd terraform ; terraform init ; terraform plan  ; terraform apply
  ```

- Update inventory

  ```
  cd terraform && terraform output |  awk -F '"' '{print $2}' >> ../ansible/inventory/hosts
  ```

  

- Provison with ansible

  ```
  cd ansible
  ansible-playbook deploy.yml
  ```

- Docker image has been pushed here : docker pull hareeshkesa/assesment:latest
  

