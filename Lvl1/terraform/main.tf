# Define provider
provider "aws" {
  region = "us-east-1"
}


data "aws_vpc" "default" {
  default = true
}
#  Get vpc id

data "aws_subnet_ids" "all" {
  vpc_id = data.aws_vpc.default.id
}

# Get AMI id

data "aws_ami" "amazon_linux" {
  most_recent = true

  owners = ["amazon"]

  filter {
    name = "name"

    values = [
      "amzn-ami-hvm-*-x86_64-gp2",
    ]
  }

  filter {
    name = "owner-alias"

    values = [
      "amazon",
    ]
  }
}

# Get Elastic Ip

resource "aws_eip" "this" {
  vpc      = true
  instance = module.ec2.id[0]
}

# security group

resource "aws_security_group" "this"{
  tags = {
    Name = "nginx security group"
  }
  name = "nginx security group"
  vpc_id = data.aws_vpc.default.id

  ingress{
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]

  }
  ingress{
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]

  }
  egress{
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# Key pair

resource "aws_key_pair" "oneclick" {
  key_name   = "oneclick"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDIpYmvTVeYENAbz9Vo7pntNxjTajejsGOoyY/MaCoCtTj6y+DQfH0T5mMLuPG7fKoYF+ZYB72daSZfOuw80ZN0vyKs3L2sV3V6Ikx8K0799Y7P+hIWNfzVy8WoxxigAuSYmx6FwaE17FnEp95tNOGyKe4Lxod8mSLrzJCRl+w2SiiJpX1I3JneOpKRrpvOsp6fzs3doe5FqDgn4WK6Oq2pxvOONnVgpMbNPU+wR6PDSmU5hzLNU1lpRR/wpY3vuixOpLK+kTVoP6fN1saysJOni322KbA26dMD4EsRulAJwXmX8njQqIbp2qeku/OwLdgW/oKsfplU71g2XjUh63Sl Key for Onclick deployment"
}

# call the ec2 module

module "ec2" {
  source = "./modules/ec2"

  instance_count = 1

  name          = "nginx-webapp"
  ami           = data.aws_ami.amazon_linux.id
  instance_type = "t2.micro"
  key_name = "oneclick"
  subnet_id     = tolist(data.aws_subnet_ids.all.ids)[0]
  vpc_security_group_ids = [aws_security_group.this.id ]
  associate_public_ip_address = true
  root_volume_size =  "20"
  root_volume_type = "gp2"

  tags = {
    "Env"      = "Webapp"
  }
}

output "public_ip" {
  value = module.ec2.public_ip
}
