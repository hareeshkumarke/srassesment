resource "aws_instance" "this" {
  count = var.instance_count

  ami                    =  var.ami 
  instance_type          =  var.instance_type 
  user_data              =  var.user_data 
  subnet_id              =  var.subnet_id 
  key_name               =  var.key_name 
  monitoring             =  var.monitoring 
  vpc_security_group_ids = var.vpc_security_group_ids 
  iam_instance_profile   =  var.iam_instance_profile 

  associate_public_ip_address =  var.associate_public_ip_address 
  ipv6_address_count          =  var.ipv6_address_count 
  ipv6_addresses              =  var.ipv6_addresses 

  ebs_optimized          =  var.ebs_optimized 
  volume_tags            =  var.volume_tags 
  root_block_device{
            volume_type = var.root_volume_type
      volume_size = var.root_volume_size
       } 

  tags = merge(var.tags, map("Name", var.instance_count > 1 ? format("%s-%d", var.name, count.index+1) : var.name))

}