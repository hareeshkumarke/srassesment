## Option2

- The module will launch an ECS cluster and will create ASG  for ECS and ALB as well. The module will output the ALB DNS name.

- For demo purposes , Nginx image is lauched as service and can be verified by opening ALB DNS Name in browser.

```
  cd terraform ; terraform init ; terraform plan  ; terraform apply
  ```

- The provided `jenkinsfile` displays a sample jenkinsfile, which will fetch code from git, build the docker image and push to ECR. This can be tirggered with a post-commit web hook.

  

  